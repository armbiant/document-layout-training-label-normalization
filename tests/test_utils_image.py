#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import cv2
import numpy as np
import pytest
from shapely.geometry import Polygon

from extractor import utils_image


@pytest.mark.parametrize(
    "input_size, output_size, expected_new_size, expected_ratio",
    [
        ((10, 10, 3), 10, (10, 10), (1, 1)),
        ((10, 10, 3), 0, (0, 0), (0, 0)),
        ((10, 10, 3), 20, (20, 20), (2, 2)),
        ((10, 10, 3), 5, (5, 5), (0.5, 0.5)),
        ((10, 6, 3), 5, (5, 3), (0.5, 0.5)),
        ((10, 7, 3), 5, (5, 3), (0.5, 0.42857142857142855)),
        ((6, 10, 3), 5, (3, 5), (0.5, 0.5)),
        ((7, 10, 3), 5, (3, 5), (0.42857142857142855, 0.5)),
        ((7, 10, 3), 6, (4, 6), (0.5714285714285714, 0.6)),
    ],
)
def test_new_image_size(input_size, output_size, expected_new_size, expected_ratio):
    new_size_h, new_size_w, ratios_h, ratios_w = utils_image.new_image_size(
        input_size, output_size
    )
    assert (new_size_h, new_size_w) == expected_new_size
    assert (ratios_h, ratios_w) == expected_ratio


@pytest.mark.parametrize(
    "elements, color, expected_image_name",
    [
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            (255, 0, 0),
            "test_1.png",
        ),
        (
            [
                Polygon([(10, 10), (20, 20), (20, 10)]),
                Polygon([(25, 15), (26, 30), (28, 19)]),
            ],
            (0, 0, 255),
            "test_2.png",
        ),
        (
            [],
            (0, 0, 255),
            "test_3.png",
        ),
    ],
)
def test_update_image(
    test_base_image, fixtures_dir, elements, color, expected_image_name
):
    expected_image = cv2.imread(os.path.join(fixtures_dir, expected_image_name))
    assert test_base_image is not None and expected_image is not None
    np.testing.assert_array_equal(
        utils_image.update_image(test_base_image, elements, color), expected_image
    )
