#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import cv2
import pytest


@pytest.fixture
def fixtures_dir():
    return os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "data",
    )


@pytest.fixture
def test_base_image(fixtures_dir):
    return cv2.imread(os.path.join(fixtures_dir, "base.png"))
