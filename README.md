# Arkindex Document Layout Training Label Normalization

## A tool to extract data from Arkindex, prepare and normalize it for image segmentation algorithms

This tool extracts and prepares datasets from Arkindex to train and evaluate image segmentation algorithms. It extracts the images, annotation images and annotations in json format. The annotations in json format can be directly used for evaluation using the [Document Image Segmentation Scoring](https://gitlab.com/teklia/dla/document_image_segmentation_scoring) tool.

The annotation images can be retrieved in a specific size and a pre-processing can be applied to the polygons to prevent them from overlapping. The whole process is described in the paper entitled "Robust Text Line Detection in Historical Documents: Learning and Evaluation Methods".

## Cloning and installation

```console
git clone https://gitlab.com/teklia/dla/arkindex_document_layout_training_label_normalization.git
cd arkindex_document_layout_training_label_normalization
pip install -e .
```

## Usage

### Authenticate

To be able to extract content from Arkindex, all the more so for private repositories, you need to authenticate. You will find your `ARKINDEX_API_TOKEN` in your personal profile, under the 'API Token' section. More details can be found in [the official Arkindex documentation.](https://doc.arkindex.org/users/auth/#personal-token)

```
export ARKINDEX_API_URL=<url_to_your_arkindex_instance>
export ARKINDEX_API_TOKEN=<you_user_id_token>
```

### Command line interface

To start extraction from Arkindex, use the following command:
```console
run-extraction --corpus <arkindex_corpus_uuid>
  --classes <class1> <class2> ...
  --colors <color1> <color2> ...
  --parents-types <parent_type1> <parent_type2> ...
  --parents-names <parent_name11> <parent_name2> ...
  --page-type <page_type>
  -i / --image-size <image_size>
  -s / --split-process
  -r / --resize-image
  --eps <eps>
```

| Parameter       | Description                                                                                                                            | Default                                                   | Optional? |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- | --------- |
| `corpus`        | UUID of the corpus from which the data will be retrieved.                                                                   |                                                           |           |
| `classes`       | List of element types that will be retrieved from Arkindex. Possible values are listed in your project information, under the tab 'Classes'.                                                                      |                                                           |           |
| `colors`        | List of RGB colors that will be used in the label images. One color for each class. Example: `0,0,255 0,255,0` if there are 2 classes. |
| `parents-types` | Type of parents of the elements. Example: `volume set`. Possible values are `volume`, `set` or `folder`.                                                                            |                                                           |           |
| `parents-names` | Names of parents of the elements. Example: `train val test`.                                                                  | All children of elements in `parent-types` are retrieved. | &check;   |
| `page-type`     | Type of the pages to retrieve.                                                                                                         | `page`                                                    | &check;   |
| `worker-version-id`     | Filter elements by a worker version ID. Elements created by other sources will not be extracted.                                                                                                         |                                                     | &check;   |
| `image-size`    | Size in which the label images will be generated.                                                                                      | Generation at original image size.                        | &check;   |
| `split-process` | Whether to separate overlapping and touching boxes.                                                                                    | False                                                     | &check;   |
| `resize-image`  | Whether to save the image in a sub-resolution (same as the label image).                                                               | False                                                     | &check;   |
| `eps`           | The higher the value, the more the polygons will be eroded. A value higher than 2 is not recommended as polygons might be removed as they become too small. | 2 | &check; |

### Example

The command below will export children of folders (`--parents-types folder`) from the corpus with uuid `corpus_uuid` on Arkindex. Only the element with types `page_footer` or `page_header` will be exported, and rendered as two colors, <span style="background-color: rgb(204,102,0)">204,102,0</span> and <span style="background-color: rgb(0,102,0)">0,102,0</span> respectively.

```
run-extraction --corpus corpus_uuid \
               --classes page_footer page_header \
               --colors 204,102,0 0,102,0 \
               --parents-types folder
```

## Public annotations

In addition to the generation code, we provide the annotation images of the public datasets used during the experiments explained in the paper. The annotations of the training and validation images of the following datasets are available:
  - Bozen
  - DIVA-HisDB
  - HOME-NACR
  - Horae
  - READ (Complex and Simple tracks)
  - cBAD2019

## Citing us

```latex
@inproceedings{boillet2022,
    author = {Boillet, Mélodie and Kermorvant, Christopher and Paquet, Thierry},
    title = {{Robust text line detection in historical documents: learning and evaluation methods}},
    booktitle = {International Journal on Document Analysis and Recognition (IJDAR)},
    year = {2022},
    month = Mar,
    pages = {1433-2825},
    doi = {10.1007/s10032-022-00395-7},
    url = {https://doi.org/10.1007/s10032-022-00395-7}
}
```
