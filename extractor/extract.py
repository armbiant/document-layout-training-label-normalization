#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The extraction module
    ======================
"""

import logging
import os

import cv2
import imageio as iio
import numpy as np
from arkindex import ArkindexClient, options_from_env
from shapely.geometry import Polygon
from tqdm import tqdm

from extractor import arkindex_utils as ark
from extractor import utils
from extractor import utils_image as ui

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


IMAGES_DIR = "./images/"  # Path to the images directory.
LABEL_IMAGES_DIR = "./labels_images/"  # Path to the label images directory.
LABEL_JSON_DIR = "./labels_json/"  # Path to the json labels directory.


def main():
    args = utils.get_cli_args()

    # Get and initialize the parameters.
    os.makedirs(IMAGES_DIR, exist_ok=True)
    os.makedirs(LABEL_IMAGES_DIR, exist_ok=True)
    os.makedirs(LABEL_JSON_DIR, exist_ok=True)

    # Login to arkindex.
    client = ArkindexClient(**options_from_env())

    subsets = ark.retrieve_subsets(
        client, args.corpus, args.parents_types, args.parents_names
    )

    # Iterate over the subsets to find the page images and labels.
    for subset in subsets:

        os.makedirs(os.path.join(IMAGES_DIR, subset["name"]), exist_ok=True)
        os.makedirs(os.path.join(LABEL_IMAGES_DIR, subset["name"]), exist_ok=True)
        os.makedirs(os.path.join(LABEL_JSON_DIR, subset["name"]), exist_ok=True)

        for page in tqdm(
            client.paginate(
                "ListElementChildren",
                id=subset["id"],
                type=args.page_type,
                recursive=True,
            ),
            desc="Set " + subset["name"],
        ):

            x_coords, y_coords = zip(*page["zone"]["polygon"])
            page_w = max(x_coords) - min(x_coords)
            page_h = max(y_coords) - min(y_coords)

            # Get the label size and initialize the label image.
            if args.image_size is not None:
                ann_h, ann_w, ratio_h, ratio_w = ui.new_image_size(
                    [page_h, page_w],
                    args.image_size,
                )
                label_image = np.zeros((ann_h, ann_w, 3), dtype=np.uint8)
            else:
                label_image = np.zeros(
                    (
                        page_h,
                        page_w,
                        3,
                    ),
                    dtype=np.uint8,
                )

            # Get the label size and initialize the json label.
            page_objects = {}
            page_objects["img_size"] = [
                page_h,
                page_w,
            ]

            for index, channel in enumerate(args.classes):
                # Filter polygons of current channel.
                polygons = [
                    element["zone"]["polygon"]
                    for element in client.paginate(
                        "ListElementChildren",
                        id=page["id"],
                        recursive=True,
                        type=channel,
                        page_size=200,
                        worker_version=args.worker_version_id,
                    )
                    if element["type"] == channel
                ]

                # Move polygons to subelement coordinates.
                polygons = [
                    [[x - min(x_coords), y - min(y_coords)] for x, y in polygon]
                    for polygon in polygons
                ]

                # Add the polygons to dict.
                page_objects[channel] = [
                    {
                        "confidence": 1.0,
                        "polygon": polygon,
                    }
                    for polygon in polygons
                ]

                # Prepare polygons for label image.
                polygons = [Polygon(poly) for poly in polygons]
                # Resize the polygons if requested.
                if args.image_size is not None:
                    polygons = utils.resize_polygons(polygons, ratio_h, ratio_w)
                # Split the polygons if requested.
                if args.split_process:
                    polygons = utils.split_polygons(polygons, args.eps)

                label_image = ui.update_image(label_image, polygons, args.colors[index])

            # Save the input image.
            image = iio.imread(page["zone"]["url"])
            if args.resize_image and args.image_size is not None:
                image = cv2.resize(image, (ann_w, ann_h))
            cv2.imwrite(
                os.path.join(IMAGES_DIR, subset["name"], f"{page['id']}.png"),
                cv2.cvtColor(image, cv2.COLOR_BGR2RGB),
            )
            # Save the labels.
            cv2.imwrite(
                os.path.join(LABEL_IMAGES_DIR, subset["name"], f"{page['id']}.png"),
                cv2.cvtColor(label_image, cv2.COLOR_BGR2RGB),
            )
            utils.save_polygons(
                page_objects,
                os.path.join(LABEL_JSON_DIR, subset["name"], f"{page['id']}.json"),
            )
