#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    The arkindex_utils module
    ======================
"""

import errno
import logging
import sys

from apistar.exceptions import ErrorResponse


def retrieve_subsets(
    client, corpus: str, parents_types: list, parents_names: list
) -> list:
    """
    Retrieve the requested subsets.
    :param client: The arkindex client.
    :param corpus: The id of the retrieved corpus.
    :param parents_types: The types of parents of the elements to retrieve.
    :param parents_names: The names of parents of the elements to retrieve.
    :return subsets: The retrieved subsets.
    """
    subsets = []
    for parent_type in parents_types:
        try:
            subsets.extend(
                client.request("ListElements", corpus=corpus, type=parent_type)[
                    "results"
                ]
            )
        except ErrorResponse as e:
            logging.error(
                f"{e.content}: corpus ({corpus}) with elements of type {parent_type}"
            )
            sys.exit(errno.EINVAL)
    # Retrieve subsets with name in parents-names. If no parents-names given, keep all subsets.
    if parents_names is not None:
        logging.info(f"Retrieving {parents_names} subset(s)")
        subsets = [subset for subset in subsets if subset["name"] in parents_names]
    else:
        logging.info("Retrieving all subsets")

    if len(subsets) == 0:
        logging.info("No subset found")

    return subsets
