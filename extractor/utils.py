#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    The utils module
    ======================
"""

import argparse
import json
import uuid
from itertools import combinations

from shapely.affinity import scale
from shapely.geometry import LineString, MultiPolygon, Polygon

MANUAL_SOURCE = "manual"


def worker_version_validator(worker_version):
    """
    Parse the worker_version_id parameter. Use 'manual' for manual source.
    Must be an UUID otherwise.
    :param worker_version: The parameter value to check.
    :return: The parsed worker_version value.
    """
    if worker_version == MANUAL_SOURCE:
        return False
    try:
        uuid.UUID(worker_version)
        return worker_version
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid UUID value: '{worker_version}'")


def uuid_validator(uuid_value: str):
    """
    Make sure the given value is a valid UUID.
    :param uuid_value: The value to check.
    :return: The same value if it's a valid UUID.
    :raises: ArgumentTypeError.
    """
    try:
        uuid.UUID(uuid_value)
        return uuid_value
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid UUID value: {uuid_value}")


def color_string(text: str) -> list:
    """
    Map a color string to a list of integers.
    :param text: The string containing the RGB code.
    :return: The list of integers.
    """
    color = list(map(int, text.split(",")))
    assert len(color) == 3, f"Invalid color {color}, should have 3 values"
    for value in color:
        assert value in range(
            256
        ), f"Invalid color {color}, values should be between 0 and 255"
    return color


def get_cli_args():
    """
    Get the command-line arguments.
    :return: The command-line arguments.
    """
    parser = argparse.ArgumentParser(
        description="Arkindex Document Layout Training Label Normalization"
    )

    # Required arguments.
    parser.add_argument(
        "--corpus",
        type=uuid_validator,
        help="UUID of the corpus from which the data will be retrieved.",
        required=True,
    )
    parser.add_argument(
        "--classes",
        nargs="+",
        type=str,
        help="List of element types that will be retrieved from Arkindex.",
        required=True,
    )
    parser.add_argument(
        "--colors",
        nargs="+",
        type=color_string,
        help="List of RGB colors that will be used in the label images. One color for each class. \
              Example: 0,0,255 0,255,0 if there are 2 classes.",
        required=True,
    )
    parser.add_argument(
        "--parents-types",
        nargs="+",
        type=str,
        help="Type of parents of the elements.",
        required=True,
    )

    # Optional arguments.
    parser.add_argument(
        "--page-type",
        type=str,
        help="Type of the pages that will be retrieved from Arkindex.",
        default="page",
    )
    parser.add_argument(
        "--worker-version-id",
        type=worker_version_validator,
        help="Filter data with a worker version ID.",
        default=None,
    )
    parser.add_argument(
        "--parents-names",
        nargs="+",
        type=str,
        help="Names of parents of the elements.",
        default=None,
    )
    parser.add_argument(
        "-i",
        "--image-size",
        type=int,
        help="Size in which the label images will be generated.",
        default=None,
    )
    parser.add_argument(
        "-r",
        "--resize-image",
        help="Whether to resize the images.",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--split-process",
        help="Whether to separate overlapping and touching boxes.",
        action="store_true",
    )
    parser.add_argument(
        "--eps",
        help="Strength of the erosion operation done on polygons",
        default=2,
        type=int,
    )
    return check_cli_args(parser.parse_args())


def check_cli_args(args):
    """
    Check that the command-line arguments are correct.
    :return args: The command-line arguments.
    """
    if args.image_size is not None:
        assert args.image_size > 0, "Invalid image_size, should be a positive integer"
    assert (
        len(args.classes) > 0
    ), "Invalid number of classes, should have at least 1 class"
    assert len(args.classes) == len(
        args.colors
    ), "Invalid number of colors, should have one color by class"
    return args


def resize_polygons(polygons: list, height: float, width: float) -> list:
    """
    Resize the polygons.
    :param polygons: The polygons to resize.
    :param height: The ratio in the height dimension.
    :param width: The ratio in the width dimension.
    :return: A list of the resized polygons.
    """
    return [
        scale(polygon, xfact=width, yfact=height, origin=(0, 0)) for polygon in polygons
    ]


def split_polygons(polygons: list, eps) -> list:
    """
    Split the touching and overlapping polygons.
    :param polygons: The polygons to split.
    :return polygons: The non-touching polygons.
    """
    for comb in combinations(range(len(polygons)), 2):
        poly1 = polygons[comb[0]]
        poly2 = polygons[comb[1]]
        # Skip invalid polygons as they cannot be compared.
        if not poly1.is_valid or not poly2.is_valid:
            continue
        # If the two polygons intersect: first erode them, then check if they still intersect.
        if poly1.intersects(poly2):
            poly1 = poly1.buffer(-eps)
            poly2 = poly2.buffer(-eps)
            intersection = poly1.intersection(poly2)
            # If they still intersect, remove the intersection from the biggest polygon.
            if not intersection.is_empty:
                if (
                    isinstance(intersection, Polygon)
                    and intersection.area < 0.2 * poly1.area
                    and intersection.area < 0.2 * poly2.area
                    or isinstance(intersection, MultiPolygon)
                ):
                    if poly1.area > poly2.area:
                        polygons[comb[0]] = poly1.difference(intersection)
                        polygons[comb[1]] = poly2.buffer(-eps)
                    else:
                        polygons[comb[1]] = poly2.difference(intersection)
                        polygons[comb[0]] = poly1.buffer(-eps)
                elif isinstance(intersection, LineString):
                    polygons[comb[0]] = poly1.difference(intersection)
                    polygons[comb[1]] = poly2.difference(intersection)
        elif poly1.touches(poly2):
            polygons[comb[0]] = poly1.buffer(-2 * eps)
            polygons[comb[1]] = poly2.buffer(-2 * eps)
    # Erode all polygons so that they don't touch when drawn over the label image.
    polygons = [poly.buffer(-2 * eps) for poly in polygons]
    return polygons


def save_polygons(polygons: list, filename: str):
    """
    Save the polygons into a json file.
    :param polygons: The polygons to save.
    :param filename: The path to the file.
    """
    with open(filename, "w") as outfile:
        json.dump(polygons, outfile, indent=4)
