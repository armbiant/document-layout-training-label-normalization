#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    The utils_image module
    ======================
"""

import cv2
import numpy as np
from shapely.geometry import Polygon


def new_image_size(input_size: tuple, output_size: int) -> (int, int, float, float):
    """
    Get the resize label size and corresponding ratios.
    The longest side of the image will have a length of output_size.
    :param input_size: The original image size.
    :param output_size: The output maximum size.
    """
    # Compute the new image size.
    ratio = float(output_size) / max(input_size[:2])
    new_size = tuple([int(x * ratio) for x in input_size[:2]])
    # Compute the ratios between the original and new image sizes.
    # It is used to resize the annotations.
    ratios = [new_size[index] / input_size[index] for index in range(len(new_size))]
    return new_size[0], new_size[1], ratios[0], ratios[1]


def update_image(ann_image: np.ndarray, elements: list, color: list) -> np.ndarray:
    """
    Add the polygons of the label image.
    :param ann_image: The label image to update.
    :param elements: The coordinates of the elements to add.
    :param color: The color of the elements to add.
    :return ann_image: The updated label image.
    """

    def int_coords(x: np.array) -> np.array:
        return np.array(x).round().astype(np.int32)

    for element in elements:
        if isinstance(element, Polygon):
            if element.is_empty:
                continue
            cv2.drawContours(
                ann_image, [int_coords(element.exterior.coords)], 0, color, -1
            )
    return ann_image
